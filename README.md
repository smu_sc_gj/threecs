# README #

This is the repository for my three C's examples, the script will be build up in a video but first I want a simple scene I can use for testing. Originally I was going to have on repository per example, but given the repartition this seems unnessesary.

## Scenes ##

#### 1. Sandbox ####
* Fixed camera
* Script to generate a tiled floor (from textured cube tiles,) and some steps.
   
   
#### 2. Movement-CharCont ####
* Fixed camera
* Floor / steps generation. 
* Player movement via the character controller

#### 3. Movement-Physics ####
* Fixed Camera
* Floor steps generation
* Player movement via physics component

#### 4. First-Person ####
* First person camera (look in direction of travel)
* Floor / steps generation
* Movement via character controller

#### 5. Mouse look ####
* FPS with mouse look (look according to mouse)
* Floor / step generation
* Movement via character controller
   
#### 6. Third Person ####
* Follow camera (very basic)
* Floor /step generation
* Movement via character controller
