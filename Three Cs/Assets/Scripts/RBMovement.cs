﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBMovement : MonoBehaviour
{
    private Rigidbody body;
    private Vector3 inputs;

    private bool isGrounded;

    private float distToGround;

    [SerializeField]
    private float speed = 10.0f;

    [SerializeField]
    private float jumpForce = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();

        distToGround = GetComponent<CapsuleCollider>().bounds.extents.y;

    }

    // Update is called once per frame
    void Update()
    {
        // Zero the input vector
        inputs = Vector3.zero;

        // Collect the inputs
        inputs.x = Input.GetAxis("Horizontal");
        inputs.z = Input.GetAxis("Vertical");

         // Look where I'm going
        if(inputs != Vector3.zero)
        {
            transform.forward = inputs;
        }

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            inputs.y = jumpForce;
        }
    }

    void FixedUpdate()
    {
        isGrounded = checkGroundContact(8);

        // Move position does not apply gravity - just does a smooth transition.
        // Ok if you have a 'fake' jump but no good for falling off steps. 
        //body.MovePosition(body.position + inputs * speed * Time.fixedDeltaTime);

        body.AddForce(inputs.x, inputs.y, inputs.z, ForceMode.Impulse);

        //Cap max speed
        float overspeed = body.velocity.magnitude - speed;
        if(overspeed > 0)
        {
            body.AddForce(body.velocity * -overspeed);
        }

    }

    bool checkGroundContact(int playerLayer)
    {
        //Work out layer mask. 
        // Bit shift the index of the player layer (8) to get the bit mask. 
        int layerMask = 1 << playerLayer;
 
        //Invert (flip) this so we're checking for collisions with every other layer
        layerMask = ~layerMask;
      
        //Raycast
        RaycastHit hit;

        if(Physics.Raycast(transform.position, Vector3.down, out hit, (distToGround+0.1f), layerMask))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
