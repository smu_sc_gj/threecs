﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPMovement : MonoBehaviour
{
	
    private CharacterController controller;

    private Vector3 velocity;

    [SerializeField] private float jumpMultiplier = 10.0f;
    [SerializeField] private AnimationCurve jumpFalloff;
    private bool isJumping = false;

    [SerializeField]
    private float speed = 10.0f;

    [SerializeField]
    public float rotationSpeed = 2.0f;

    [SerializeField]
    private float gravity = -9.8f;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController> ();
    
        jumpFalloff = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
 
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 move = Vector3.zero;

        if(controller.isGrounded)
            velocity.y = 0.0f;

		//The Horizontal/Vertical are bound project wide to a selection of keys.
		float deltaY = Input.GetAxis ("Horizontal") * rotationSpeed;
		float deltaZ = Input.GetAxis ("Vertical") * speed;

		if(deltaY != 0 || deltaZ != 0)
		{
			//Assign vector for movement update.
            // - my camera points along the z-axis
			move.z  = deltaZ;

			//clamp x,z and xz movement.
			move = Vector3.ClampMagnitude(move,speed);

			//update orientation
            Quaternion update = Quaternion.AngleAxis(deltaY, Vector3.up);
			transform.rotation *= update;

            // convert from local space to world space
		    // move = transform.TransformDirection (move);
            move =  transform.rotation * move;

            //scale movement to framerate            
            move *= Time.deltaTime;

            //update the controller
		    controller.Move (move);
		}

        // Apply gravity 
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if(Input.GetButtonDown("Jump") && controller.isGrounded && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }

    IEnumerator JumpEvent()
    {
        float timeInAir = 0.0f;

        do
        {
            float jumpForce = jumpFalloff.Evaluate(timeInAir);

            controller.Move(Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime);
            timeInAir += Time.deltaTime;

            yield return null;

        } while (
            !controller.isGrounded &&
            controller.collisionFlags != CollisionFlags.Above
        );

        isJumping = false;
    }

}
