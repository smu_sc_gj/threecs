﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPCameraMouseLook : MonoBehaviour
{
    private float xAxisRotation;
    
    [SerializeField] 
    private float mouseSensitivity = 100;

    /* For FPS mouse look 90 / -90 seem OK, stops you seeing your body, 
    for TPS chase cam with mouse look you need to constrain the view 
    a little more - depending on the feel you want (20 / -10) */

    [SerializeField]
    private bool clampMouseLook = true;

    [SerializeField] 
    private float lookUpLimit = 90.0f;

    [SerializeField] 
    private float lookDownLimit = -90.0f;
    
    private Transform playerBody;

    // Awake is called before start (even if the object is disabled)
    // use this to connect up the player. 
    void Awake()
    {
        playerBody = transform.parent.GetComponent<Transform>();
        //playerBody = GetComponentInParent<Transform>();

        // Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        xAxisRotation = 0.0f;
    }

    // Start is called before the first frame update
    void Start()
    {    
        
    }

    // Update is called once per frame
    void Update()
    {
        // Handle camera rotation

        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xAxisRotation += mouseY;

        
        if(clampMouseLook)
        {
            // Clamp accumulated x rotation.
            if (xAxisRotation > lookUpLimit)
            {  
                mouseY +=  lookUpLimit - xAxisRotation;
                xAxisRotation = lookUpLimit;
            }
            else if (xAxisRotation < lookDownLimit)
            {
                mouseY += lookDownLimit - xAxisRotation;
                xAxisRotation = lookDownLimit;
            }
        }

        // Rotate the camera (up/down)
        transform.Rotate(Vector3.left * mouseY);

        // Rotate the players body (left/right)
        playerBody.Rotate(Vector3.up * mouseX);
    }
}
