﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look : MonoBehaviour
{
    private Transform playerBody;
    private float xAxisRotation;

    [SerializeField]
    private float mouseSens = 100.0f;

    // Awake is called before Start (even if inactive)
    void Awake()
    {
        playerBody = transform.parent.GetComponent<Transform>();

        // Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    // Start is called before the first frame update
    void Start()
    {
        xAxisRotation = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        // Get mouse input
        float mouseX = Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime;

        xAxisRotation += mouseY;

        // CLamp rotation around x
        if(xAxisRotation > 90.0f)
        {
            mouseY += 90.0f - xAxisRotation;
            xAxisRotation = 90.0f;
        }
        else if(xAxisRotation < -90.0f)
        {
            mouseY += -90.0f - xAxisRotation;
            xAxisRotation = -90.0f;
        }
        
        // Apply mouse y to x rotation of camera
        transform.Rotate(Vector3.left * mouseY);

        // Apply mouse x to y rotation of player
        playerBody.Rotate(Vector3.up * mouseX);
    }
}
