﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 velocity;

    // Jump stuff
    private bool isJumping = false;

    [SerializeField]
    private float jumpMultiplier = 10.0f;

    [SerializeField]
    private AnimationCurve jumpFalloff;

    [SerializeField]
    private float gravity = -9.8f;

    [SerializeField]
    private float forwardSpeed = 10.0f;


    [SerializeField]
    private float straifSpeed = 10.0f;

    // Awake is called before start 
    void Awake()
    {
        controller = GetComponent<CharacterController>();

        if(jumpFalloff == null)
            jumpFalloff = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
    }

    // Start is called before the first frame update
    void Start()
    {
        velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        // Movement update
        Vector3 movement = Vector3.zero;

        // Cancel negative velocity if grounded
        if(controller.isGrounded)
            velocity.y = 0.0f;

        // Get input from Input system
        float deltaX = Input.GetAxis("Horizontal");
        float deltaZ = Input.GetAxis("Vertical");

        movement.z = deltaZ * forwardSpeed;
        movement.x = deltaX * straifSpeed;

        // Clamp magnitude of vector 
        Vector3.ClampMagnitude(movement, forwardSpeed);

        // Rotate vector into player space
        movement = transform.rotation * movement;


        // Smooth movement using timedelta
        movement *= Time.deltaTime;

        // Move character controller
        controller.Move(movement);

        // Increase velocity due to gravity
        velocity.y += gravity * Time.deltaTime;
        
        // Apply gravity
        controller.Move(velocity * Time.deltaTime);

        // Jump
        if(Input.GetButtonDown("Jump") && controller.isGrounded && !isJumping)
        {
           isJumping = true;
           StartCoroutine(JumpEvent());
        }

    }

    IEnumerator JumpEvent()
    {
        float timeInAir = 0.0f;

        do
        {
            float jumpForce = jumpFalloff.Evaluate(timeInAir);

            controller.Move(Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime);

            timeInAir += Time.deltaTime;

            yield return null;

        } while (
            !controller.isGrounded &&
            controller.collisionFlags != CollisionFlags.Above
        );

        isJumping = false;
    }
}
